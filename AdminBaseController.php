<?php

namespace app\controllers;

use app\models\PageFilters;
use app\models\PageWidgets;
use app\models\WidgetClinics;
use app\modules\dbconversion\models\Service;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class AdminBaseController extends Controller {

    const DEFAULT_AJAX_PAGE_LIMIT = 30;

    public function beforeAction($action) {

        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors() {

        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return string
     */
    public function actionAttachWidgets($id){
        $pageModel = $this::findModel($id);
        $model = new PageWidgets();
        if ($model->load(Yii::$app->request->post())){



            $model->model_translit = get_class($pageModel);
            $model->model_id = $id;

            $options = [];
            if(Yii::$app->request->post()['sort']){
                $model->sort = Yii::$app->request->post()['sort'];
            }
            if(Yii::$app->request->post()['PageWidgets']['options']['doctors']){
                $options['doctors'] = Yii::$app->request->post()['PageWidgets']['options']['doctors'];
            }
            if(Yii::$app->request->post()['PageWidgets']['options']['clinics']){
                $options['clinics'] = Yii::$app->request->post()['PageWidgets']['options']['clinics'];
            }
            if(Yii::$app->request->post()['PageWidgets']['options']['services']){
                $options['services'] = Yii::$app->request->post()['PageWidgets']['options']['services'];
            }

            if(!empty($options)){
                $model->options = json_encode($options);
            }

            $model->save(false);

            switch($model->widget_id){
                // Если редактируется виджет сравнения цен услуг в клиника
                case "tablica_cen_in_clinics":

                    // Если клиники не выбраны вручную
                    $empty = true;
                    foreach(Yii::$app->request->post()['PageWidgets']['options']['clinics'] as $post_clinic){
                        if(!empty($post_clinic['id'])){
                            $empty = false;
                            break;
                        }
                    }

                    if($empty){
                        if(get_class($pageModel) == 'app\models\Service'){
                            $service = \app\models\Service::findOne($id);
                            // Находим клиники по этой услуги у которых заполнены цены
                            // Сортируем по у кого само мало ссылок в других виджетах
                            $clinics = $service->getClinicServicesDefault();

                            if(!empty($clinics)){
                                $i = 1;
                                foreach($clinics as $clinic){
                                    $widget_clinic = new WidgetClinics();
                                    $widget_clinic->widget_id = $model->id;
                                    $widget_clinic->clinic_id = $clinic->clinic->id;
                                    $widget_clinic->sort = $i;
                                    $widget_clinic->save();

                                    $i++;
                                }
                            }
                        }
                    }else{
                        $i = 1;
                        foreach(Yii::$app->request->post()['PageWidgets']['options']['clinics'] as $post_clinic){
                            if(!empty($post_clinic['id'])){
                                $widget_clinic = new WidgetClinics();
                                $widget_clinic->widget_id = $model->id;
                                $widget_clinic->clinic_id = $post_clinic['id'];
                                $widget_clinic->sort = $i;
                                $widget_clinic->save();

                                $i++;
                            }
                        }
                    }
            }

            $this->redirect('/'.Yii::$app->controller->getUniqueId().'/update?id='.$id);

        }

        return $this->render('/widgets/attachWidget', [
            'model' => $model,
            'serviceModel' => $model
        ]);
    }

    /**
     * Удаляет связь в виджете
     */
    public function actionRemoveRelationInWidget($id, $name, $relation_id){

        WidgetClinics::deleteAll(['id' => $relation_id]);

        $model = PageWidgets::findOne($id);

        $options = (json_decode($model->options)) ? :[];
        $options->{$name}->{$relation_id} = "";


        $model->options = json_encode($options);

        return $model->save(false);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionUpdateWidget($id){
        $model = PageWidgets::findOne($id);
        $pageModel = $this::findModel($model->model_id);

        $doctors = (isset(json_decode($model->options)->doctors))?json_decode($model->options)->doctors:[];
        $clinics = (isset(json_decode($model->options)->clinics))?json_decode($model->options)->clinics:[];
        $services = (isset(json_decode($model->options)->services))?json_decode($model->options)->services:[];

        if ($model->load(Yii::$app->request->post())){


            $options = [];
            if(Yii::$app->request->post()['sort']){
                $model->sort = Yii::$app->request->post()['sort'];
            }
            if(isset(Yii::$app->request->post()['PageWidgets']['options']['doctors'])){
                if(!empty($doctors)){
                    foreach(Yii::$app->request->post()['PageWidgets']['options']['doctors'] as $key => $doctor){
                        if(!empty(Yii::$app->request->post()['PageWidgets']['options']['doctors'][$key]['id'])){
                            $options['doctors'][$key] = $doctor;
                        }else{
                            $options['doctors'][$key] = (isset($doctors->{$key})) ? $doctors->{$key} : null;
                        }

                    }
                }else{
                    $options['doctors'] = Yii::$app->request->post()['PageWidgets']['options']['doctors'];
                }

            }
            if(isset(Yii::$app->request->post()['PageWidgets']['options']['clinics'])){



                if(!empty($clinics)){
                    foreach(Yii::$app->request->post()['PageWidgets']['options']['clinics'] as $key => $clinic){
                        if(!empty(Yii::$app->request->post()['PageWidgets']['options']['clinics'][$key]['id'])){
                            $options['clinics'][$key] = $clinic;
                        }else{
                            $options['clinics'][$key] = (isset($clinics->{$key})) ? $clinics->{$key} : null;
                        }

                    }
                }else{
                    $options['clinics'] = Yii::$app->request->post()['PageWidgets']['options']['clinics'];
                }

            }
            if(isset(Yii::$app->request->post()['PageWidgets']['options']['services'])){

                if(!empty($services)){
                    foreach(Yii::$app->request->post()['PageWidgets']['options']['services'] as $key => $service){

                        if(!empty(Yii::$app->request->post()['PageWidgets']['options']['services'][$key]['id'])){
                            $options['services'][$key] = $service;
                        }else{
                            $options['services'][$key] = (isset($services->{$key})) ? $services->{$key} : null;

                            if(!empty(Yii::$app->request->post()['PageWidgets']['options']['services'][$key]['title']) && (isset($services->{$key}))){
                                $options['services'][$key]->title = $service['title'];
                            }
                        }

                    }
                }else{
                    $options['services'] = Yii::$app->request->post()['PageWidgets']['options']['services'];
                }


            }
            if(!empty($options)){
                $model->options = json_encode($options);
            }


            $model->save(false);

            switch($model->widget_id){
                // Если редактируется виджет сравнения цен услуг в клиника
                case "tablica_cen_in_clinics":

                    // Если клиники не выбраны вручную
                    $empty = true;
                    foreach(Yii::$app->request->post()['PageWidgets']['options']['clinics'] as $key => $post_clinic){
                        if(!empty($post_clinic['id'])){
                            $empty = false;


                            $widget_clinic = new WidgetClinics();
                            $widget_clinic->widget_id = $model->id;
                            $widget_clinic->clinic_id = $post_clinic['id'];
                            $widget_clinic->sort = $key;
                            $widget_clinic->save();
                        }
                    }

                    if($empty){
                        foreach(Yii::$app->request->post()['PageWidgets']['options']['clinics'] as $key =>$post_clinic){
                            if(!empty($post_clinic['id'])){
                                $widget_clinic = new WidgetClinics();
                                $widget_clinic->widget_id = $model->id;
                                $widget_clinic->clinic_id = $post_clinic['id'];
                                $widget_clinic->sort = $key;
                                $widget_clinic->save();

                            }
                        }
                    }
            }

            $this->redirect('/'.Yii::$app->controller->getUniqueId().'/update?id='.$model->model_id);
;
        }


        return $this->render('/widgets/updateWidget', [
            'model' => $model,
            'serviceModel' => $pageModel
        ]);
    }

    public function actionAttachFilter($id){
        $serviceModel = $this::findModel($id);

        $model = new PageFilters();
        if ($model->load(Yii::$app->request->post())){
            $model->model_translit = get_class($serviceModel);
            $model->model_id = $id;

            $options = [];
            if(isset(Yii::$app->request->post()['title'])){
                $options['title'] = Yii::$app->request->post()['title'];
            }
            if(!empty($options)){
                $model->options = json_encode($options);
            }


            $model->save();

            $this->redirect('/service/update/'.$id);

        }

        return $this->render('/filters/attachFilter', [
            'model' => $model
        ]);
    }

    public function actionUpdateFilter($id){
        $model = PageFilters::findOne($id);

        if ($model->load(Yii::$app->request->post())){
            $options = [];
            if(Yii::$app->request->post()['title']){
                $options['title'] = Yii::$app->request->post()['title'];
            }
            if(!empty($options)){
                $model->options = json_encode($options);
            }


            $this->redirect('/service/update/'.$model->model_id);

            $model->save();

        }

        return $this->render('/filters/updateFilter', [
            'model' => $model
        ]);
    }


}
