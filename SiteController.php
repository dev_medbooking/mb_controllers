<?php

namespace app\controllers;

use app\models\DoctorService;
use app\models\Service;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use rico\yii2images\models\Image;

class SiteController extends AdminBaseController
{

    public function beforeAction($action) {
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['logout'],
                    'rules' => [
                        [
                            'actions' => ['logout','test'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'logout' => ['post'],
                    ],
                ],
            ]
        );
    }



    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//    public function actionIndex()
//    {
//        return $this->render('index');
//    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /** remove image by ajax */
    public function actionDeleteImage()
    {
        /** @var Image $image */
        if (!empty($id = Yii::$app->request->post('id'))) {
            $image = Image::findOne($id);
            if ($image && $image->delete()) {
                //Todo:: if deletion ok
            }
        }
        echo '{}';
    }

    public function actionGetSelectData(){
        if(!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        switch(Yii::$app->request->get('type')){
            case 'services':
                $query = \app\models\Service::find()->select('id, title as text')->orderBy(['title'=>SORT_ASC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'title', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;

            case 'doctors':

                $query = \app\models\Doctor::find()->joinWith('categories',false);

                if($service_id = Yii::$app->request->get('service_id')){

                    $query->joinWith('services', false);
                    $query->andWhere(['doctor_service.service_id' => $service_id]);
                }
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'CONCAT_WS(\' \',`surname`,`name`,`patronymic`)', Yii::$app->request->get('query')]);
                $total_count = $query->count();

                $expression = new \yii\db\Expression("CONCAT_WS(' ',`".\app\models\Doctor::tableName()."`.`surname`,`".\app\models\Doctor::tableName()."`.`name`,`".\app\models\Doctor::tableName()."`.`patronymic`) as text, GROUP_CONCAT(`".\app\models\Category::tableName()."`.`title` ORDER BY `".\app\models\Category::tableName()."`.`title` ASC SEPARATOR ', ') as qt");
                $query->select([\app\models\Doctor::tableName().".id",$expression])
                    ->orderBy([\app\models\Doctor::tableName().'.surname'=>SORT_ASC])
                    ->groupBy(\app\models\Doctor::tableName().'.id');

                break;

            case 'clinics':
                $query = \app\models\Clinic::find()->select('clinic.id, clinic.title as text')->orderBy(['clinic.title'=>SORT_ASC]);

                if($service_id = Yii::$app->request->get('service_id')){
                    $query->joinWith('services', false);
                    $query->andWhere(['clinic_service.service_id' => $service_id]);
                }
                $query->andWhere(['clinic.status' => 1]);
                if(Yii::$app->request->get('query')) {
                    $query->andWhere(['like', 'clinic.title', Yii::$app->request->get('query')]);
                }
                $total_count = $query->count();
                break;

            case 'links':
                $query = \app\models\Meta::find()->select('id, url as text')->orderBy(['url'=>SORT_ASC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'url', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;

            case 'blogs':
                $query = \app\models\Blog::find()->select('id, title as text')->orderBy(['publication_date'=>SORT_DESC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'title', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;

            case 'articles':
                $query = \app\models\Article::find()->select('id, title as text')->orderBy(['publication_date'=>SORT_DESC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'title', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;

            case 'diseases':
                $query = \app\models\Disease::find()->select('id, title as text')->orderBy(['title'=>SORT_ASC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'title', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;

            case 'symptoms':
                $query = \app\models\Symptom::find()->select('id, title as text')->orderBy(['title'=>SORT_ASC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'title', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;

            case 'faqs':
                $query = \app\models\Faq::find()->select('id, title as text')->orderBy(['updated_at'=>SORT_DESC]);
                if(Yii::$app->request->get('query')) $query->andWhere(['like', 'title', Yii::$app->request->get('query')]);
                $total_count = $query->count();
                break;
        }

        $page_limit = Yii::$app->request->get('page_limit')?:self::DEFAULT_AJAX_PAGE_LIMIT;

        return [
            'items' => $query->offset((Yii::$app->request->get('page')?:0)*$page_limit)->limit($page_limit)->asArray()->all(),
            'total_count' => $total_count,
            'page_limit' => $page_limit
        ];
    }
}
