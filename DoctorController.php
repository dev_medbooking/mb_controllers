<?php

namespace app\controllers;

use app\models\ClinicDoctor;
use app\models\DoctorCategory;
use app\models\DoctorService;
use rico\yii2images\models\Image;
use synatree\dynamicrelations\DynamicRelations;
use Yii;
use app\models\Doctor;
use app\models\search\SearchDoctor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DoctorController implements the CRUD actions for Doctor model.
 */
class DoctorController extends AdminBaseController {

    public function behaviors() {
        return array_merge(parent::behaviors(), []
        );
    }

    /**
     * Lists all Doctor models.
     * @return mixed
     */
    public function actionIndex() {
        if (!\Yii::$app->user->can('doctorView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchDoctor();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing DoctorService  model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteDoctorService($id) {
        if (!\Yii::$app->user->can('doctorUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $isDelete = DoctorService::findOne($id)->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($isDelete) {
                $success = [
                    'message' => 'Связь доктором с услугой успешно удалена',
                    'response' => 1
                ];
            } else {
                $success = [
                    'message' => 'Ошибка удаления связи',
                    'response' => 0
                ];
            }

            return $success;
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Deletes an existing DoctorCategory  model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteDoctorCategory($id) {
        if (!\Yii::$app->user->can('doctorUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $isDelete = DoctorCategory::findOne($id)->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($isDelete) {
                $success = [
                    'message' => 'Связь доктора со специализацией успешно удалена',
                    'response' => 1
                ];
            } else {
                $success = [
                    'message' => 'Ошибка удаления связи с доктором',
                    'response' => 0
                ];
            }

            return $success;
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Displays a single Doctor model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        if (!\Yii::$app->user->can('doctorView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Doctor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('doctorCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Doctor();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            DynamicRelations::relate($model, 'doctorCategories', Yii::$app->request->post(), 'DoctorCategory', DoctorCategory::className());
            DynamicRelations::relate($model, 'doctorServices', Yii::$app->request->post(), 'DoctorService', DoctorService::className());
            DynamicRelations::relate($model, 'clinicDoctors', Yii::$app->request->post(), 'ClinicDoctor', ClinicDoctor::className());
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Doctor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!\Yii::$app->user->can('doctorUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (empty($model->errors) && $model->save()) {
                    DynamicRelations::relate($model, 'doctorCategories', Yii::$app->request->post(), 'DoctorCategory', DoctorCategory::className());
                    DynamicRelations::relate($model, 'doctorServices', Yii::$app->request->post(), 'DoctorService', DoctorService::className());
                    DynamicRelations::relate($model, 'clinicDoctors', Yii::$app->request->post(), 'ClinicDoctor', ClinicDoctor::className());

                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Doctor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\Yii::$app->user->can('doctorDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');
        $model = $this->findModel($id);
        if ($model) {
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Doctor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Doctor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
