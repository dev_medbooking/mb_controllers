<?php

namespace app\controllers;

use Yii;
use app\models\Blog;
use app\models\search\SearchBlog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use rico\yii2images\models\Image;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends AdminBaseController {

    public function behaviors() {
        return array_merge(parent::behaviors(), []
        );
    }

    public function beforeAction($action) {
        $allowedActions = [
            'create', 'update'
        ];
        if (in_array($action->id, $allowedActions)) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex() {
        if (!\Yii::$app->user->can('blogView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchBlog();
        $searchModel->is_blog = 1;
        $dataProvider = $searchModel->searchBlog(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'is_blog' => true
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!\Yii::$app->user->can('blogView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('blogCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Blog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setService();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);
        if (!\Yii::$app->user->can('blogUpdate', $model))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        if ($model->load(Yii::$app->request->post()))
            if ($model->validate())
                if (empty($model->errors) && $model->save()) {
                    $model->setService();
//                    return $this->redirect(['update', 'id' => $model->id]);
                }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\Yii::$app->user->can('blogDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
