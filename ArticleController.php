<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\search\SearchBlog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class ArticleController extends AdminBaseController
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [ ]
        );
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->can('articleView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchBlog();
        $searchModel->is_blog = 0;
        $dataProvider = $searchModel->searchArticle(Yii::$app->request->queryParams);

        return $this->render('//blog/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'is_blog' => false
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!\Yii::$app->user->can('articleView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('//blog/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->can('articleCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setService();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('//blog/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can('articleUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
            if ($model->validate())
                if (empty($model->errors) && $model->save()) {
                    $model->setService();
//                    return $this->redirect(['update', 'id' => $model->id]);
                }

        return $this->render('//blog/update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can('articleDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
