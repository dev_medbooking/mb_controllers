<?php

namespace app\controllers;

use Yii;
use app\models\Symptom;
use app\models\SymptomCases;
use app\models\search\SearchSymptom;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use synatree\dynamicrelations\DynamicRelations;

/**
 * SymptomsController implements the CRUD actions for Symptoms model.
 */
class SymptomController extends AdminBaseController {


    /**
     * Lists all Symptoms models.
     * @return mixed
     */
    public function actionIndex() {
//        if (!\Yii::$app->user->can('symptomView'))
//            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchSymptom();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Symptoms model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!\Yii::$app->user->can('symptomView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Symptoms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('symptomCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Symptom();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setService();
            $model->setDisease();
            DynamicRelations::relate($model, 'cases', Yii::$app->request->post(), 'SymptomCases', SymptomCases::className());
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Symptoms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!\Yii::$app->user->can('symptomUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
            if ($model->validate())
                if (empty($model->errors) && $model->save()) {
                    $model->setService();
                    $model->setDisease();
                    DynamicRelations::relate($model, 'cases', Yii::$app->request->post(), 'SymptomCases', SymptomCases::className());
//                    return $this->redirect(['update', 'id' => $model->id]);
                }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Symptoms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\Yii::$app->user->can('symptomDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing FaqAnswers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteCases($id) {
        if (!\Yii::$app->user->can('symptomUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModelCases($id)->delete();

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        } else {
            return "OK";
        }
    }

    /**
     * Finds the FaqAnswers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FaqAnswers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelCases($id) {
        if (($model = SymptomCases::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Symptoms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Symptoms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Symptom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
