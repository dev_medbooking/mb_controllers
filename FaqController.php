<?php

namespace app\controllers;

use Yii;
use app\models\Faq;
use app\models\FaqAnswers;
use app\models\search\SearchFaq;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use synatree\dynamicrelations\DynamicRelations;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends AdminBaseController {

    public function behaviors() {
        return array_merge(parent::behaviors(), []
        );
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex() {
        if (!\Yii::$app->user->can('faqView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchFaq();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!\Yii::$app->user->can('faqView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('faqCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Faq();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setService();
            DynamicRelations::relate($model, 'answers', Yii::$app->request->post(), 'FaqAnswers', FaqAnswers::className());
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!\Yii::$app->user->can('faqUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setService();
            DynamicRelations::relate($model, 'answers', Yii::$app->request->post(), 'FaqAnswers', FaqAnswers::className());
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\Yii::$app->user->can('faqDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing FaqAnswers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteAnswer($id) {
        if (!\Yii::$app->user->can('faqUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModelAnswer($id)->delete();

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        } else {
            return "OK";
        }
    }

    /**
     * Finds the FaqAnswers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FaqAnswers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelAnswer($id) {
        if (($model = FaqAnswers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
