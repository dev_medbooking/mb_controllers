<?php

namespace app\controllers;

use app\components\helpers\HelperDump;
use app\models\ClinicDoctor;
use app\models\ClinicMode;
use app\models\ClinicService;
use app\models\ClinicSubway;
use app\models\Doctor;
use app\models\DoctorService;
use app\models\WidgetClinics;
use rico\yii2images\models\Image;
use synatree\dynamicrelations\DynamicRelations;
use Yii;
use app\models\Clinic;
use app\models\search\SearchClinic;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClinicController implements the CRUD actions for Clinic model.
 */
class ClinicController extends AdminBaseController {

    public function behaviors() {
        return array_merge(parent::behaviors(), []
        );
    }

    /**
     * Lists all Clinic models.
     * @return mixed
     */
    public function actionIndex() {
        if (!\Yii::$app->user->can('clinicView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchClinic();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clinic model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        \Yii::$app->params['entityId'] = $id;
        if (!\Yii::$app->user->can('clinicView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Clinic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('clinicCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Clinic();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->post('ClinicDoctor')) {
                DynamicRelations::relate($model, 'clinicDoctors', Yii::$app->request->post(), 'ClinicDoctor', ClinicDoctor::className());
            }
            if (Yii::$app->request->post('ClinicService')) {
                DynamicRelations::relate($model, 'clinicServices', Yii::$app->request->post(), 'ClinicService', ClinicService::className());
            }
            if (Yii::$app->request->post('ClinicSubway')) {
                DynamicRelations::relate($model, 'clinicSubways', Yii::$app->request->post(), 'ClinicSubway', ClinicSubway::className());
            }
            if (Yii::$app->request->post('ClinicMode')) {
                DynamicRelations::relate($model, 'clinicMode', Yii::$app->request->post(), 'ClinicMode', ClinicMode::className());
            }

            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Clinic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {

        if (!\Yii::$app->user->can('clinicUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);
        $clinicSubwaysOld = $model->getClinicSubways()
            ->select('subway_id')
            ->asArray(true)
            ->createCommand()
            ->queryColumn()
        ;
        $clinicPointOld = [
            'lat' => $model->lat,
            'lng' => $model->lng,
        ];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (Yii::$app->request->post('ClinicService')) {
                DynamicRelations::relate($model, 'clinicServicesAll', Yii::$app->request->post(), 'ClinicService', ClinicService::className());
                // если стоит галочка пакетного сейва
                if($model->pack_save){
                    //для каждой измененной услуги
                    foreach(Yii::$app->request->post('ClinicService') as $csid => $cs) {
                        // если клиника сетевая
                        if($model->is_network){
                            //обновляем цену во всех клиниках сети
                            $clinic_ids = \yii\helpers\ArrayHelper::getColumn(Clinic::find()->where(['network_id'=>$model->id])->asArray()->all(), 'id');
                            $cli_data = [
                                'price'=> $cs['price'],
                                'discount' => $cs['discount'],
                                'is_start_price' => $cs['is_start_price'],
                                'in_euro' => $cs['in_euro'],
                                'brand_only' => $cs['brand_only'],
                            ];
                            ClinicService::updateAll($cli_data, ['service_id'=>$cs['service_id'], 'clinic_id' =>$clinic_ids]);
                            //добавляем так же головную клинику в массив для обновления врачей
                            $clinic_ids[] = $model->id;
                        } else {
                            // если не сетевая, то нас интересует только эта клиника для обновления врачей
                            $clinic_ids = $model->id;
                        }
                        //обновляем цены у врачей
                        $doc_data = [
                            'price'=> $cs['price'],
                            'discount' => $cs['discount'],
                            'is_start_price' => $cs['is_start_price'],
                            'in_euro' => $cs['in_euro']
                        ];
                        if($cs['price']>0) $doc_data['is_free'] = 0;
                        DoctorService::updateAll($doc_data, ['service_id'=>$cs['service_id'], 'clinic_id' =>$clinic_ids]);
                    }
                }
            }
            if (Yii::$app->request->post('ClinicMode')) {
                DynamicRelations::relate($model, 'clinicMode', Yii::$app->request->post(), 'ClinicMode', ClinicMode::className());
            }
            if (Yii::$app->request->post('ClinicDoctor')) {
                DynamicRelations::relate($model, 'clinicDoctors', Yii::$app->request->post(), 'ClinicDoctor', ClinicDoctor::className());
            }
            if (Yii::$app->request->post('ClinicSubway')) {
                DynamicRelations::relate($model, 'clinicSubways', Yii::$app->request->post(), 'ClinicSubway', ClinicSubway::className());
            }

            if(Yii::$app->request->post()['Clinic']['is_hidden'] == 1){
                // Удаляем клинику из виджетов
                WidgetClinics::deleteAll(['clinic_id' => $model->id]);
            }

            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clinic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\Yii::$app->user->can('clinicDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        //don't delete clinics =) there is no delete status yet
//        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing ClinicDoctor  model.
     * If deletion  is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteClinicDoctor($id) {
        if (!\Yii::$app->user->can('clinicUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $clinicDoctor = ClinicDoctor::findOne($id);
        $clinicDoctorServiceWhere = ['doctor_id'=>$clinicDoctor->doctor_id, 'clinic_id'=>$clinicDoctor->clinic_id];
        $isDelete = $clinicDoctor->delete() && DoctorService::deleteAll($clinicDoctorServiceWhere);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($isDelete) {
                $success = [
                    'message' => 'Связь клиники с доктором успешно удалена',
                    'response' => 1
                ];
            } else {
                $success = [
                    'message' => 'Ошибка удаления связи с доктором',
                    'response' => 0
                ];
            }

            return $success;
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Deletes an existing ClinicDoctor  model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteClinicService($id) {
        if (!\Yii::$app->user->can('clinicUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');
        $isDelete = ClinicService::findOne($id)->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($isDelete) {
                $success = [
                    'message' => 'Связь клиники с услугой ' . $id . ' успешно удалена',
                    'response' => 1
                ];
            } else {
                $success = [
                    'message' => 'Ошибка удаления связи с услугой ' . $id,
                    'response' => 0
                ];
            }

            return $success;
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Deletes an existing ClinicSubway  model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteClinicSubway($id) {
        if (!\Yii::$app->user->can('clinicUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');


        $isDelete = ClinicSubway::findOne($id)->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($isDelete) {
                $success = [
                    'message' => 'Связь клиники с метро успешно удалена',
                    'response' => 1
                ];
            } else {
                $success = [
                    'message' => 'Ошибка удаления связи с метро',
                    'response' => 0
                ];
            }

            return $success;
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the Clinic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Clinic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Clinic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
