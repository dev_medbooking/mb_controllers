<?php

namespace app\controllers;

use app\models\ClinicService;
use app\models\DoctorService;
use app\models\PageFilters;
use app\models\PageWidgets;
use app\models\Widgets;
use rico\yii2images\models\Image;
use synatree\dynamicrelations\DynamicRelations;
use Yii;
use app\models\Service;
use app\models\search\SearchService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends AdminBaseController {

    public function behaviors() {
        return array_merge(parent::behaviors(), []
        );
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex() {
        if (!\Yii::$app->user->can('serviceView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        if (!\Yii::$app->user->can('serviceView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\Yii::$app->user->can('serviceCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Service();
        $model->lvl = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // link relations in model afterSave() method
            DynamicRelations::relate($model, 'clinicServices', Yii::$app->request->post(), 'ClinicService', ClinicService::className());
            DynamicRelations::relate($model, 'doctorServices', Yii::$app->request->post(), 'DoctorService', DoctorService::className());
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!\Yii::$app->user->can('serviceUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (empty($model->errors) && $model->save()) {
                    // link relations in model afterSave() method
                    DynamicRelations::relate($model, 'clinicServices', Yii::$app->request->post(), 'ClinicService', ClinicService::className());
                    DynamicRelations::relate($model, 'doctorServices', Yii::$app->request->post(), 'DoctorService', DoctorService::className());
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }



    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\Yii::$app->user->can('serviceDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (!$model = Service::findOne($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

}
