<?php

namespace app\controllers;

use app\models\Article;
use app\models\Blog;
use app\models\Disease;
use app\models\Faq;
use app\models\Symptom;
use Yii;
use app\models\Advert;
use app\models\search\SearchAdvert;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdvertController implements the CRUD actions for Advert model.
 */
class AdvertController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Advert models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->can('advertView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchAdvert();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advert model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!\Yii::$app->user->can('advertView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advert model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->can('advertCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Advert();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setPages();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Advert model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can('advertUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setPages();
//            return $this->redirect(['update', 'id' => $model->id]);
        }

        $defaults = \yii\helpers\ArrayHelper::index(\app\models\AdvertPage::find()->where(['advert_id'=>$model->id, 'default'=>\app\models\base\BaseConst::STATUS_ACTIVE])->all(), 'model');
        if(isset($defaults[Blog::className()])) $model->blogs_default = 1;
        if(isset($defaults[Article::className()])) $model->articles_default = 1;
        if(isset($defaults[Symptom::className()])) $model->symptoms_default = 1;
        if(isset($defaults[Disease::className()])) $model->diseases_default = 1;
        if(isset($defaults[Faq::className()])) $model->faqs_default = 1;

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Advert model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can('advertDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Advert model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advert the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advert::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
