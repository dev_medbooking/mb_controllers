<?php

namespace app\controllers;

use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\components\widgets\ShowRelateEntity;

class HelperController extends AdminBaseController {

    public function beforeAction($action) {
        return parent::beforeAction($action);
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout', 'doctor', 'clinic', 'service', 'subway', 'translit', 'add-elem', 'search-term'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
                ]
        );
    }

    /**
     * Ajax
     * Авто транслит полей - урлы
     * @param type $str
     * @return type
     */
    public function actionTranslit($str) {
        if (!Yii::$app->request->isAjax) {
            return;
        }
        return \app\components\Slug::slug($str);
    }

    /**
     * Search models to Autocomplete
     * @var array
     */
    public static $modelsSearch = [
        'clinic' => '\app\models\Clinic',
        'clinic-service' => '\app\models\Clinic',
        'clinic-doctor' => '\app\models\Clinic',
        'clinic-subway' => '\app\models\Clinic',
        // relation doctor
        'doctor-category' => '\app\models\Doctor',
        'doctor-clinic' => '\app\models\Doctor',
        'doctor-service' => '\app\models\Doctor',
        // relation services
        'service-clinic' => '\app\models\Service',
        'service-doctor' => '\app\models\Service',
    ];

    /**
     * Search models filter dataProvider
     * @var array
     */
    public static $models = [
        'clinic' => '\app\models\Clinic',
        'clinic-service' => '\app\models\ClinicService',
        'clinic-doctor' => '\app\models\ClinicDoctor',
        'clinic-subway' => '\app\models\ClinicSubway',
        // relation doctor
        'doctor-category' => '\app\models\DoctorCategory',
        'doctor-clinic' => '\app\models\ClinicDoctor',
        'doctor-service' => '\app\models\DoctorService',
        // relation services
        'service-clinic' => '\app\models\ClinicService',
        'service-doctor' => '\app\models\DoctorService',
    ];

    // relation clinic
    const CLINIC = 'clinic';
    const CLINIC_SERVICE = 'clinic-service';
    const CLINIC_DOCTOR = 'clinic-doctor';
    const CLINIC_SUBWAY = 'clinic-subway';
    // relation doctor
    const DOCTOR_CATEGORY = 'doctor-category';
    const DOCTOR_CLINIC = 'doctor-clinic';
    const DOCTOR_SERVICE = 'doctor-service';
    // relation services
    const SERVICE_CLINIC = 'service-clinic';
    const SERVICE_DOCTOR = 'service-doctor';

    public function actionAddElem($type) {
        $class = isset(self::$models[$type]) ? self::$models[$type] : false;
        if (!$class) {
            throw new \yii\web\NotFoundHttpException();
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $uniqId = uniqid();

        $view = '@app/components/widgets/views/_item_relate_new';
        $model = new $class();
        $dataClass = explode('\\', $class);
        $classNameRelate = array_pop($dataClass);
        $frag = $classNameRelate . "[new][$uniqId]";

        $dataView = [
            'model' => $model,
            'uniq' => $uniqId,
            'frag' => $frag,
            'classNameRelate' => $classNameRelate,
            'urlDelete' => '#',
        ];
        switch ($type) {
            // relation clinic
            case self::CLINIC_SERVICE:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-service';
                $dataView['attrs'] = [
                    'service_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemService'], // callable
                        'name' => $frag . '[service_id]',
                        'wrapClass' => 'col-lg-3',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'name' => $frag . '[price]',
                        'wrapClass' => 'col-lg-2'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'name' => $frag . '[discount]',
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'name' => $frag . '[is_start_price]',
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'name' => $frag . '[in_euro]',
                        'wrapClass' => 'col-lg-2'
                    ],
                    'brand_only' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'name' => $frag . '[brand_only]',
                        'wrapClass' => 'col-lg-2'
                    ],
                ];

                break;
            case self::CLINIC_DOCTOR:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-service';
                $dataView['attrs'] = [
                    'fio' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'name' => $frag . '[fio]',
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemDoctor'], // callable
                        'wrapClass' => 'col-lg-3',
                    ],
                    'speciality_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'name' => $frag . '[speciality_id]',
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemSpeciality'], // callable
                        'wrapClass' => 'col-lg-2',
                        'note' => 'Доступна после сохранения связи'
                    ],
                    'position' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'name' => $frag . '[position]',
                        'wrapClass' => 'col-lg-2'
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'name' => $frag . '[home_visit]',
                        'wrapClass' => 'col-lg-2'
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'name' => $frag . '[child]',
                        'wrapClass' => 'col-lg-1'
                    ],
                ];
                break;
            case self::CLINIC_SUBWAY:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-service';
                $dataView['attrs'] = [
                    'subway_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'name' => $frag . '[subway_id]',
                        'wrapClass' => 'col-lg-6',
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemSuybway'], // callable
                        'note' => 'Доступна после сохранения связи',
                    ],
                    'time_on_foot' => [
                        'wrapClass' => 'col-lg-3',
                        'itemClass' => 'js-time_on_foot',
                    ],
                    'time_by_bus' => [
                        'wrapClass' => 'col-lg-3',
                        'itemClass' => 'js-time_by_bus',
                    ],
                ];
                $dataView['actionButton'] = [
                    'name' => 'пересчитать удалённость до метро',
                    'options' => [
                        'class' => 'btn btn-sm btn-info pull-right js-clinic-subway-recount js-new-item',
                    ],
                ];

                break;
            // relation doctor
            case self::DOCTOR_CATEGORY:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-doctor-category';
                $dataView['attrs'] = [
                    'category_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemCategory'],
                        'wrapClass' => 'col-lg-4',
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-3',
                    ],
                    'rate' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-3',
                    ],
                ];
                break;
            case self::DOCTOR_CLINIC:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-doctor-category';
                $dataView['attrs'] = [
                    'clinic_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemClinic'],
                        'wrapClass' => 'col-lg-3',
                    ],
                    'speciality_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemSpeciality'],
                        'wrapClass' => 'col-lg-2',
                    ],
                    'position' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2',
                    ],
                    'note' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2',
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1',
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1',
                    ],
                ];
                break;
            case self::DOCTOR_SERVICE:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-doctor-category';
                $dataView['attrs'] = [
                    'service_id' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemService'],
                        'wrapClass' => 'col-lg-2',
                    ],
                    'clinic_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'wrapClass' => 'col-lg-2',
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemClinic'], // callable
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'price_info' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'is_free' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                ];
                break;
            // relation services
            case self::SERVICE_CLINIC:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-service-clinic';
                $dataView['attrs'] = [
                    'clinic_id' => [
                        'type' => 'text',
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemClinic'],
                        'wrapClass' => 'col-lg-3',
                        'attrValue' => 'id',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'brand_only' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                ];
                break;
            case self::SERVICE_DOCTOR:
                $dataView['isDeleteButton'] = true;
                $dataView['cssClassDeleteButton'] = 'remove-relation-item-service-doctor';
                $dataView['attrs'] = [
                    'fio' => [
                        'type' => 'text',
                        'relation' => 'doctor',
                        'itemsType' => 'call_user_func',
                        'items' => ['app\components\widgets\ShowRelateEntity', 'getListItemDoctor'],
                        'wrapClass' => 'col-lg-3',
                        'attrValue' => 'id',
                    ],
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'clinic',
                        'wrapClass' => 'col-lg-2',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-3'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_free' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                ];
                break;
        }

        return [
            'uniqId' => $uniqId,
            'content' => $this->renderAjax($view, $dataView)
        ];
    }

    /**
     * Jquery Autocomplete Ajax query handler
     * @param type $type
     * @param type $term
     * @param type $id
     * @return type
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSearchTerm($type, $term, $id) {

        $class = isset(self::$modelsSearch[$type]) ? self::$modelsSearch[$type] : false;
        if (!$class) {
            throw new \yii\web\NotFoundHttpException('Класс {' . $class . '} не найден в массиве классов static $models');
        }
        if (!$model = $class::findOne($id)) {
            throw new \yii\web\NotFoundHttpException('Модель с #ID ' . $class . ':' . $id . ' не найдена');
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        switch ($type) {
            // relation clinic
            case self::CLINIC:
            case self::CLINIC_SERVICE:
                $entity = 'service';
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->getClinicServicesSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/clinic/delete-clinic-service';
                $attrs = [
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'service',
                        'wrapClass' => 'col-lg-3',
                        'attrValue' => 'id',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'brand_only' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'service_id' => [
                        'type' => 'hidden',
                        'wrapClass' => 'hidden'
                    ],
                ];
                break;
            case self::CLINIC_DOCTOR:
                $entity = 'doctor';
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->getClinicDoctorsSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/clinic/delete-clinic-doctor';
                $attrs = [
                    'fio' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'doctor',
                        'wrapClass' => 'col-lg-3',
                        'attrValue' => 'id',
                    ],
                    'speciality_id' => [
                        'type' => ShowRelateEntity::TYP_SELECT,
                        'wrapClass' => 'col-lg-3',
                        'items' => \yii\helpers\ArrayHelper::map($model ? $model->getCategories()->all() : [], 'id', 'title'),
                        'note' => 'Доступна после сохранения связи'
                    ],
//        'clinic_id' => [
//            'type' => ShowRelateEntity::TYP_SELECT,
//            'wrapClass' => 'col-lg-3',
//            'items' => \yii\helpers\ArrayHelper::map($model->find()->all(), 'id', 'title'),
//            'note' => 'Доступна после сохранения связи'
//        ],
                    'position' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                ];
                break;
            case self::CLINIC_SUBWAY:
                $entity = self::CLINIC_SUBWAY;
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->getClinicSubwaySearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/doctor/delete-clinic-subway';
                $attrs = [
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'subway',
                        'wrapClass' => 'col-lg-3',
                    ],
                    'subway_id' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-3',
                        'attrValue' => 'subway_id',
                    ],
                ];

                break;
            // relation doctor
            case self::DOCTOR_CATEGORY://
                $entity = self::DOCTOR_CATEGORY;
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->doctorCategoriesSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/doctor/delete-doctor-category';
                $attrs = [
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'category',
                        'wrapClass' => 'col-lg-4',
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-3',
                    ],
                    'rate' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-3',
                    ],
                ];
                break;
            case self::DOCTOR_CLINIC:
                $entity = 'doctor-clinic';
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->doctorClinicSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/clinic/delete-clinic-doctor';
                $attrs = [
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'clinic',
                        'wrapClass' => 'col-lg-3',
                    ],
                    'specialityTitle' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2',
                    ],
                    'position' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2',
                    ],
                    'note' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2',
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1',
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1',
                    ],
                ];
                break;

            case self::DOCTOR_SERVICE:
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->doctorServiceSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/doctor/delete-doctor-service';
                $entity = 'doctor-service';
                $attrs = [
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'service',
                        'wrapClass' => 'col-lg-2',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'price_info' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'is_free' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                ];
                break;
            case self::SERVICE_DOCTOR:
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->getServiceDoctorSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/doctor/delete-doctor-service';
                $entity = 'service-doctor';
                $attrs = [
                    'fio' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'doctor',
                        'wrapClass' => 'col-lg-2',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'price_info' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'home_visit' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'child' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'is_free' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                ];

                break;
            case self::SERVICE_CLINIC:
                $dataProvider = new yii\data\ActiveDataProvider([
                    'query' => $model->getServicesClinicSearch($term),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                $urlDelete = '/clinic/delete-clinic-service';
                $entity = 'service-clinic';
                $attrs = [
                    'title' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'relation' => 'clinic',
                        'wrapClass' => 'col-lg-3',
                        'attrValue' => 'id',
                    ],
                    'price' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'discount' => [
                        'type' => ShowRelateEntity::TYP_TEXT,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'is_start_price' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-1'
                    ],
                    'in_euro' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                    'brand_only' => [
                        'type' => ShowRelateEntity::TYP_CHECKBOX,
                        'wrapClass' => 'col-lg-2'
                    ],
                ];

                break;
        }
        if (!$entity) {
            $entity = explode('-', $type)[1];
        }
        return [
            'content' => ShowRelateEntity::widget([
                'entity' => $entity,
                'modeWidget' => $type,
                'isAddButton' => false,
                'urlDelete' => $urlDelete,
                'modelNameRelate' => '\app\models\ClinicService',
                'modelAutocomplete' => '\app\models\Service',
                'isHandleSearchAutocomplete' => false,
                'dataProvider' => $dataProvider,
                'typeRelateKey' => $type,
                'cssClassDeleteButton' => 'remove-relation-item-' . $entity,
                'cssClassAddButton' => 'add-relation-item-' . $entity,
                'cssAddDataContainerAjax' => 'container-pjax-' . $entity,
                'model' => $model,
                'attrs' => $attrs,
            ])
        ];
    }

    /**
     * Jquery Ui autocomplete
     * @param type $type
     * @return type
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\UnknownMethodException
     */
    public function actionAutocomplete($type) {
        $class = isset(self::$models[$type]) ? self::$models[$type] : false;
        if (!$class) {
            throw new \yii\web\NotFoundHttpException();
        }
        if (!method_exists(new $class(), 'getAutocomplete')) {
            throw new \yii\base\UnknownMethodException('Отсутствует метод {getAutocomplete} в модели ' . $class);
        }
        return (new $class())->getAutocomplete($type);
    }

}
