<?php

namespace app\controllers;

use Yii;
use app\models\Meta;
use app\models\MetaLinks;
use app\models\search\SearchMeta;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use synatree\dynamicrelations\DynamicRelations;

/**
 * MetaController implements the CRUD actions for Meta model.
 */
class MetaController extends AdminBaseController
{
    /**
     * Lists all Meta models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->can('metaView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $searchModel = new SearchMeta();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Meta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!\Yii::$app->user->can('metaView'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Meta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->can('metaCreate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = new Meta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            $model->setLink();
            DynamicRelations::relate($model, 'link', Yii::$app->request->post(), 'MetaLinks', MetaLinks::className());
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Meta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can('metaUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            $model->setLink();
            DynamicRelations::relate($model, 'pageLink', Yii::$app->request->post(), 'MetaLinks', MetaLinks::className());
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Meta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can('metaDelete'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing MetaLinks  model.
     * If deletion  is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteLink($id) {
        if (!\Yii::$app->user->can('clinicUpdate'))
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен.');

        MetaLinks::findOne($id)->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Meta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
